package com.example.techtestapp

import android.app.Activity
import android.content.ContentValues.TAG
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import com.example.techtestapp.databinding.ActivityLoginBinding
import com.facebook.*
import com.facebook.login.Login
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.tasks.Task
import org.json.JSONException
import java.util.*


class LoginActivity : AppCompatActivity() {

    lateinit var binding:ActivityLoginBinding
    lateinit var callback:CallbackManager
    private val responseLauncher = registerForActivityResult(ActivityResultContracts.StartActivityForResult()){ activityResult ->
        if ( activityResult.resultCode == Activity.RESULT_OK) {
            val task: Task<GoogleSignInAccount> = GoogleSignIn.getSignedInAccountFromIntent(activityResult.data)
            handleSignInResult(task)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityLoginBinding.inflate(layoutInflater)
        setContentView(binding.root)

        //set the title for toolbar
        binding.loginToolbar.setTitle("Bienvenido")

        //define the google autheticator
        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestEmail()
            .build()

        val mGoogleSignInClient = GoogleSignIn.getClient(this, gso)


        //define the facebook authenticator
        callback = CallbackManager.Factory.create()

        LoginManager.getInstance().registerCallback(callback,
            object : FacebookCallback<LoginResult?> {
                override fun onSuccess(loginResult: LoginResult?) {

                    val profile = Profile.getCurrentProfile()

                    if (profile != null){

                        val img_url = profile.getProfilePictureUri(512,512).toString()
                        val name = profile.name
                        val token_id = profile.id
                        saveSession(name, img_url, token_id, "facebook")
                        startActivity(Intent(binding.root.context, MainActivity::class.java))
                        finish()

                    }else{

                        Toast.makeText(binding.root.context, "Error al Iniciar Sesión, Vuelve a intentarlo", Toast.LENGTH_LONG).show()

                    }

                }

                override fun onCancel() {

                    Toast.makeText(binding.root.context, "Se cancelo el incicio de sesión", Toast.LENGTH_LONG).show()

                }

                override fun onError(exception: FacebookException) {

                    Toast.makeText(binding.root.context, "Error: ${exception.message}", Toast.LENGTH_LONG).show()

                }
            })

        // click listener for google auth button
        binding.loginGoogleButton.setOnClickListener(View.OnClickListener {
            val signInIntent = mGoogleSignInClient.signInIntent
            responseLauncher.launch(signInIntent)


        })

        //clicl listeber for the facebook auth button
        binding.facebookLoginButton.setOnClickListener(View.OnClickListener {
            LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile", "email"));
        })


    }

    private fun handleSignInResult(completedTask: Task<GoogleSignInAccount>) {
        try {

            val account: GoogleSignInAccount = completedTask.getResult(ApiException::class.java)
            // Signed in successfully, show authenticated UI.

            val uri = account.photoUrl.toString()
            val token_id = account.id.toString()
            val name:String = account.displayName.toString()

            saveSession(name, uri, token_id, "google")
            Toast.makeText(this, "¡Inicio de Sesión Exitoso!!", Toast.LENGTH_LONG).show()
            passToMainActivity()


        } catch (e: ApiException) {

            // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.
            Log.w(TAG, "signInResult:failed code=" + e.message)
            Toast.makeText(this, "No se puso Iniciar Sesión Correctamente Error: ${e.message}", Toast.LENGTH_LONG).show()

        }
    }

    private fun saveSession(name:String, url:String, token_id:String, type:String){

        //Do a sharedpreference with the session for show the img name and email of the user
        val preferences: SharedPreferences.Editor? = this.getSharedPreferences("user_info", MODE_PRIVATE).edit()
        preferences?.putString("name", name)
        preferences?.putString("img_url", url)
        preferences?.putString("token_id", token_id)
        preferences?.putString("type", type)
        preferences?.apply()

    }

    private fun passToMainActivity(){
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
        finish()
    }

}