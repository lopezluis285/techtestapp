package com.example.techtestapp.model

import java.io.Serializable

data class ItemModel(val title:String, val url_img:String, val description:String):Serializable
