package com.example.techtestapp

import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.google.android.gms.auth.api.signin.GoogleSignIn

class SplashActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        //verify if inside shared preferences have a token_id go to MainActivity else go to LoginActivity
        var preferences = getSharedPreferences("user_info", MODE_PRIVATE)
        var token_id = preferences.getString("token_id", null)

        if (token_id == null){

            startActivity(Intent(this, LoginActivity::class.java))
            finish()

        }else{

            startActivity(Intent(this, MainActivity::class.java))
            finish()

        }

    }

}