package com.example.techtestapp.ui.my_items

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import com.example.techtestapp.databinding.FragmentMyItemsBinding
import com.example.techtestapp.viewmodel.MyItemsViewModel

class MyItemsFragment : Fragment() {

    private var _binding:FragmentMyItemsBinding? = null

    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val myItemsViewModel = ViewModelProvider(this).get(MyItemsViewModel::class.java)

        _binding = FragmentMyItemsBinding.inflate(inflater, container, false)
        val root: View = binding.root

        return root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}