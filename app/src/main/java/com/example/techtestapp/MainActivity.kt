package com.example.techtestapp

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.BitmapFactory
import android.os.Bundle
import android.view.Menu
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.res.ResourcesCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.example.techtestapp.databinding.ActivityMainBinding
import com.facebook.login.LoginManager
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.material.navigation.NavigationView
import java.net.URL
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors

class MainActivity : AppCompatActivity() {

    private lateinit var appBarConfiguration: AppBarConfiguration
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setSupportActionBar(binding.appBarMain.toolbar)

        val preferences = getSharedPreferences("user_info", MODE_PRIVATE)
        val drawerLayout: DrawerLayout = binding.drawerLayout
        val navView: NavigationView = binding.navView
        val navController = findNavController(R.id.nav_host_fragment_content_main)

        appBarConfiguration = AppBarConfiguration(
            setOf(
                R.id.nav_home,
                R.id.nav_settings,
                R.id.nav_profile,
                R.id.nav_my_items
            ), drawerLayout)

        setupActionBarWithNavController(navController, appBarConfiguration)
        navView.setupWithNavController(navController)

        //Set the profile info
        setProfileInfo(navView, preferences)

        //sign out listener
        val signOut = binding.navView.menu.findItem(R.id.nav_sign_out)
        signOut.setOnMenuItemClickListener {

            val type = preferences.getString("type", null)

            when (type){
                "google" -> googleSignOut(this)
                "facebook" -> facebookSignOut(this)
                else -> Toast.makeText(this, "Error para Cerrar Sesión", Toast.LENGTH_LONG).show()
            }

            true

        }

    }

    private fun facebookSignOut(context: Context) {

        LoginManager.getInstance().logOut()
        clearPreferences()
        startActivity(Intent(context, LoginActivity::class.java))
        finish()

    }

    private fun googleSignOut(context: Context) {

        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestEmail()
            .build()
        val mGoogleSignInClient = GoogleSignIn.getClient(context, gso)

        mGoogleSignInClient.signOut()
            .addOnCompleteListener(this) {

                clearPreferences()
                startActivity(Intent(context, LoginActivity::class.java))
                finish()
            }

    }

    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.nav_host_fragment_content_main)
        return navController.navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
    }

    fun clearPreferences(){

        val preferences: SharedPreferences.Editor = getSharedPreferences("user_info", MODE_PRIVATE).edit()
        preferences.clear()
        preferences.apply()

    }

    fun setProfileInfo(navView: NavigationView, preferences: SharedPreferences){

        //Set the profile info in the nav drawer
        val view = navView.getHeaderView(0)
        view.findViewById<TextView>(R.id.nav_drawer_text_view).text = preferences.getString("name", null)

        //Set the profile img into the imageview
        if (preferences.getString("img_url", null) != "null"){
            val url = URL(preferences.getString("img_url", null))
            val executor: ExecutorService = Executors.newSingleThreadExecutor()
            executor.execute {
                val bitmap = BitmapFactory.decodeStream(url.openConnection().getInputStream())
                runOnUiThread {
                    view.findViewById<ImageView>(R.id.nav_drawer_photo_view).setImageBitmap(bitmap)
                }
            }
        }else{

            val imageResource = resources.getIdentifier("@drawable/ic_launcher_foreground", null, packageName)
            val res = ResourcesCompat.getDrawable(resources,imageResource, null)
            view.findViewById<ImageView>(R.id.nav_drawer_photo_view).setImageDrawable(res)
        }
    }

}

