package com.example.techtestapp.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.techtestapp.model.ItemModel
import com.example.techtestapp.model.ItemProvider
import com.facebook.internal.Mutable

class HomeViewModel : ViewModel() {

    fun getItemsData(): List<ItemModel> {
        return ItemProvider.getItems()
    }

}