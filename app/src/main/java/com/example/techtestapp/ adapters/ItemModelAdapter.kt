package com.example.techtestapp.adapters

import android.content.Context
import android.content.Intent
import android.content.Intent.FLAG_ACTIVITY_NEW_TASK
import android.graphics.drawable.Drawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.example.techtestapp.R
import com.example.techtestapp.ShowItemActivity
import com.example.techtestapp.model.ItemModel
import java.io.Serializable

class ItemModelAdapter(private val context: Context, private val items:List<ItemModel>): RecyclerView.Adapter<ItemModelAdapter.ItemModelViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemModelViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.item_row, parent, false)
        return ItemModelViewHolder(view)
    }

    override fun onBindViewHolder(holder: ItemModelViewHolder, position: Int) {
        val item = items[position]
        holder.bindView(item)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    inner class ItemModelViewHolder(view: View): RecyclerView.ViewHolder(view) {

        val imgView = view.rootView.findViewById<ImageView>(R.id.item_row_image)
        val title = view.rootView.findViewById<TextView>(R.id.item_row_title)
        val progressBar = view.rootView.findViewById<ProgressBar>(R.id.progress_bar_item)
        val constraint = view.rootView.findViewById<ConstraintLayout>(R.id.constraint_layout_card_view)

        fun bindView(item:ItemModel){

            title.text = item.title
            Glide.with(context)
                .load(item.url_img)
                .listener( object: RequestListener<Drawable>{
                    override fun onLoadFailed(
                        e: GlideException?,
                        model: Any?,
                        target: Target<Drawable>?,
                        isFirstResource: Boolean
                    ): Boolean {
                        progressBar.visibility = View.GONE
                        return false
                    }

                    override fun onResourceReady(
                        resource: Drawable?,
                        model: Any?,
                        target: Target<Drawable>?,
                        dataSource: DataSource?,
                        isFirstResource: Boolean
                    ): Boolean {
                        progressBar.visibility = View.GONE
                        return false
                    }

                }

            ).into(imgView)

            constraint.setOnClickListener {

                val intent = Intent(it.rootView.context, ShowItemActivity::class.java).setFlags(
                    FLAG_ACTIVITY_NEW_TASK)
                intent.putExtra("item", item as Serializable)
                it.rootView.context.startActivity(intent)
            }

        }
    }

}

