package com.example.techtestapp

import android.Manifest
import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.drawable.Drawable
import android.location.Location
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.example.techtestapp.databinding.ActivityShowItemBinding
import com.example.techtestapp.model.ItemModel
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng

class ShowItemActivity : AppCompatActivity(), OnMapReadyCallback{

    lateinit var binding:ActivityShowItemBinding
    lateinit var map:GoogleMap
    lateinit var fusedLocationClient: FusedLocationProviderClient

    companion object {
        const val REQUEST_CODE_LOCATION = 1
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityShowItemBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setSupportActionBar(binding.toolbarShowItem)
        supportActionBar?.apply {
            title = "Info. Item"
            setDisplayHomeAsUpEnabled(true)
            setDisplayHomeAsUpEnabled(true)
        }

        createmapFragment()

        val item = intent.extras?.get("item") as ItemModel
        binding.itemShowTitle.text = item.title
        binding.itemShowDescription.text = item.description

        Glide.with(this)
            .load(item.url_img)
            .listener(object : RequestListener<Drawable>{
                override fun onLoadFailed(
                    e: GlideException?,
                    model: Any?,
                    target: Target<Drawable>?,
                    isFirstResource: Boolean
                ): Boolean {
                    binding.itemShowProgressBar.visibility = View.GONE
                    return false
                }

                override fun onResourceReady(
                    resource: Drawable?,
                    model: Any?,
                    target: Target<Drawable>?,
                    dataSource: DataSource?,
                    isFirstResource: Boolean
                ): Boolean {
                    binding.itemShowProgressBar.visibility = View.GONE
                    return false
                }

            })
            .into(binding.itemShowImg)

            binding.buyButton.setOnClickListener{
                buyEvent(this)
            }




    }

    private fun buyEvent(context: Context) {

        val builder = AlertDialog.Builder(context)
        builder.setIcon(R.drawable.ic_warning_svgrepo_com)
        builder.setTitle("Aviso")
        builder.setMessage("¿Está seguro que desea realizar la compra?")
        builder.setPositiveButton("Aceptar"){
                _,_ ->
            Toast.makeText(context, "¡Su compra se ha realizado con Éxito!", Toast.LENGTH_LONG).show()
            onBackPressed()
        }
        builder.setNegativeButton("Cancelar", null)
        builder.show()

    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    private fun createmapFragment() {
        val mapFragment  = supportFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)

    }

    override fun onMapReady(googleMap: GoogleMap) {
        map = googleMap
        enableLocation()

    }

    private fun isLocationPermisisionGranted() = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION ) == PackageManager.PERMISSION_GRANTED

    private fun enableLocation(){
        if (!::map.isInitialized) return
        if (isLocationPermisisionGranted()){
            map.isMyLocationEnabled = true
            fusedLocationClient.lastLocation.addOnSuccessListener(this) { location ->
                animateCamera(location)
            }
        }else{
            requestLocationPermission()
        }
    }

    private fun animateCamera(location: Location?) {
        if (location != null){
            val currentLatLong = LatLng(location.latitude, location.longitude)
            map.animateCamera(CameraUpdateFactory.newLatLngZoom(currentLatLong, 12f))
        }
    }


    private fun requestLocationPermission(){
        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)){
            Toast.makeText(this, "Ve a Ajustes y Acepta los Permisos", Toast.LENGTH_LONG).show()
        }else{
            ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), REQUEST_CODE_LOCATION)
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when(requestCode){
            REQUEST_CODE_LOCATION -> if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                map.isMyLocationEnabled = true
                fusedLocationClient.lastLocation.addOnSuccessListener(this) { location ->
                    animateCamera(location)
                }
            }else{
                Toast.makeText(this, "Ve a Ajustes y Acepta los Permisos", Toast.LENGTH_LONG).show()
            }
            else -> {}
        }
    }

    override fun onResumeFragments() {
        super.onResumeFragments()
        if (!::map.isInitialized) return
        if (!isLocationPermisisionGranted()){
            map.isMyLocationEnabled = false
            fusedLocationClient.lastLocation.addOnSuccessListener(this) { location ->
                animateCamera(location)
            }
            Toast.makeText(this, "Ve a Ajustes y Acepta los Permisos", Toast.LENGTH_LONG).show()

        }
    }

}